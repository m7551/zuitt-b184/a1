package com.zuitt.wdc044_s01.controllers;

import com.zuitt.wdc044_s01.models.Post;
import com.zuitt.wdc044_s01.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin //enable cross origin request via @CrossOrigin
public class PostController {

    @Autowired
    PostService postService;

//    get all posts
    @RequestMapping(value="/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPost(){
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

//    create a new post
    @RequestMapping(value="/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){
        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

//    update an existing post
    @RequestMapping(value = "/posts/{postid}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long postid, @RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){
        return postService.updatePost(postid, stringToken, post);
    }

//  delete an existing post
    @RequestMapping(value = "/posts/{postid}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long postid, @RequestHeader(value = "Authorization") String stringToken){
        return postService.deletePost(postid, stringToken);
    }


//    get posts of specific user
    @RequestMapping(value="/myPosts", method = RequestMethod.GET)
    public ResponseEntity<Object> getMyPosts(@RequestHeader(value = "Authorization") String stringToken){
        return new ResponseEntity<>(postService.getMyPosts(stringToken), HttpStatus.OK);
    }

}
