package com.zuitt.wdc044_s01.models;

import javax.persistence.*;

@Entity
//Entity represents a table. Mark this Java object as a database table

//name the table
@Table(name="posts")
public class Post {
//    indicate the primary key
    @Id
//    auto-increment the ID column
    @GeneratedValue
    private Long id;

    @Column
    private String title;

    @Column
    private String content;

    @ManyToOne
    @JoinColumn(name="user_id", nullable = false)
    private User user;

//    default constructor needed when retrieving posts
    public Post(){}
//    other necessary constructors
    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
