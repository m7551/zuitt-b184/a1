package com.zuitt.wdc044_s01.repositories;

import com.zuitt.wdc044_s01.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends CrudRepository<Post, Object> {
}

//In Express, data goes through the process of route -> controller -> model -> database
//In Spring Boot, data goes through the process of controller -> service -> repository -> model -> database